# Socio-economic Impact Tool

### Description

(also wiki link?)

### Wireframe

The wireframe for the project is available here: https://app.diagrams.net/#G1uL8taHmMBVP4BQMWwgjvv8e_hqPfRhQX#%7B%22pageId%22%3A%22DxCF8aZOM_kGBYS2OKD-%22%7D

<img src="src/assets/images/h&t_wireframe.drawio.svg" alt="" width="100%" height="600">

Note that you will need to be logged into the Data and Technology Google Account to view it.

#TODO: Make the wireframe more available, mayb export it as an image and store it in the repository.

## Running the application

`skaffold dev`

Also `toolbox enter yarn`, `yarn install`, `yarn build`, `yarn start`.

In general a lot of this project will be more easily interacted with in the `yarn` toolbox. There are utility tools such as `yarn generate` to create a new component and (eventually) linters.

## Tests

Todo, will likely eventually be `yarn test` and `yarn test.watch`
