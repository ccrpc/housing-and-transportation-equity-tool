import { Polygon, MultiPolygon, Feature } from "geojson";
import { MapGeoJSONFeature } from "maplibre-gl";
import { booleanIntersects } from "@turf/turf";
import { Map } from "maplibre-gl";
import { extent } from "geojson-bounds";

// Define the return type
interface ScreenshotResult {
  filteredFeatures: MapGeoJSONFeature[];
  map_area_screenshot: string;
}

/**
 * Returns a promise that will fire the next time the map is idle.
 *
 * See also https://docs.mapbox.com/mapbox-gl-js/api/map/#map.event:idle
 * @param map The mapboxgl map to wait on
 * @returns
 */
export function waitForIdle(map: Map): Promise<void> {
  return new Promise((resolve) => {
    map.once("idle", () => {
      resolve();
    });
  });
}

/**
 * Returns a list of features that intersect with the buffer and a screenshot of the map.
 *
 * @param map A reference to the map being used
 * @param featureLayers A list of layers from which to get the features from
 * @param buffer A buffer for which intersection will be tested
 * @returns An object containing the filtered features and the screenshot
 */
export async function findIntersectingFeatures(
  map: Map,
  featureLayers: string[],
  buffer: Feature<Polygon | MultiPolygon>
): Promise<ScreenshotResult> {
  if (!buffer) {
    return;
  }

  // We disable user controls here so that the zoom process cannot be interrupted
  map.dragPan.disable();
  map.scrollZoom.disable();
  map.doubleClickZoom.disable();
  map.keyboard.disable();
  const previousBounds = map.getBounds();

  // Zoom so that the entire buffer is rendered
  map.fitBounds(extent(buffer), { linear: false });
  await waitForIdle(map);

  const features: MapGeoJSONFeature[] = map.queryRenderedFeatures({
    layers: featureLayers,
  });

  map.fitBounds([previousBounds.getSouthWest(), previousBounds.getNorthEast()]);
  await waitForIdle(map);

  // Take a screenshot after the map has stopped moving
  const map_area_screenshot = await takeScreenshot(map);

  // Now that we have the information we need we can re-enable user controls
  map.dragPan.enable();
  map.scrollZoom.enable();
  map.keyboard.enable();
  map.doubleClickZoom.enable();

  const filteredFeatures = features.filter((feature) =>
    booleanIntersects(buffer, feature.geometry)
  );

  return {
    filteredFeatures,
    map_area_screenshot,
  };
}

/**
 * Creates and returns a screenshot of the map, the entire rendered area.
 * @param map The mapbox-gl map of which to take the screenshot
 * @returns the base64Url of the image, as a png
 */
function takeScreenshot(map: Map): Promise<string> {
  return new Promise((resolve) => {
    // We trigger on a render event as otherwise the image will be empty due to mapbox-gl using a drawing buffer.
    map.once("render", () => {
      resolve(map.getCanvas().toDataURL());
    });
    (map as any)._render(); // Causes the map to render. We are using (map as any) as the _render() function is not publicly exposed. If there is a publicly exposed equivalent, change this.
  });
}
