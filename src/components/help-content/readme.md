# help-content



<!-- Auto Generated Below -->


## Dependencies

### Depends on

- [ht-modal](../modal)

### Graph
```mermaid
graph TD;
  help-content --> ht-modal
  ht-modal --> ion-header
  ht-modal --> ion-toolbar
  ht-modal --> ion-title
  ht-modal --> ion-buttons
  ht-modal --> ion-button
  ht-modal --> ion-icon
  ht-modal --> ion-content
  ion-button --> ion-ripple-effect
  style help-content fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
