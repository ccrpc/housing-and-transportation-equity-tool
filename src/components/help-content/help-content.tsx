import { h, Component } from "@stencil/core";

@Component({
  styleUrl: "help-content.css",
  tag: "help-content",
})
export class HelpContent {
  render() {
    return [
      <ht-modal name="How to use this tool">
        <h2>Put Content for how to use this tool here</h2>
      </ht-modal>,
    ];
  }
}
