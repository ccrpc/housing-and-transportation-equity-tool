import {
  Component,
  State,
  Prop,
  Listen,
  h,
  Element,
  Watch,
} from "@stencil/core";

// External Libraries
// Drawing polygons on map
import { DrawOptions } from "@ccrpc/webmapgl/dist/types/components/draw-controller/interface";

// To define type of array for buildings in the polygon
import { MapGeoJSONFeature } from "maplibre-gl";

// To get the features of the polygon
import { Feature } from "@ccrpc/webmapgl";

// Utility Functions
import { findIntersectingFeatures } from "../../utils";

import { Polygon, MultiPolygon } from "geojson";

// Define valid units for Turf.js
type Units = "meters" | "kilometers" | "miles" | "feet";

@Component({
  tag: "app-root",
  styleUrl: "app-root.css",
  shadow: false,
})
export class AppRoot {
  @Element() el: HTMLElement;

  // Properties
  @Prop() segmentTiles =
    "https://maps.ccrpc.org/access-score/baseline-2020.json";
  @Prop() lat = 40.1207;
  @Prop() long = -88.2337;
  @Prop() zoom = 10.96;

  // States
  @State() drawing: boolean = false;
  @State() selectedSegments: any[] = [];
  @State() drawnFeatures: Feature[] = [];
  @State() selectedSource: string = "road-segments";
  @State() displayBuffer: boolean = false;
  @State() intersectingBuildingsWithUnion: MapGeoJSONFeature[] = [];
  @State() bufferSize: number = 100.0;
  @State() bufferUnit: Units = "meters";
  @State() bufferArea: string = "";
  @State() map_screenshot_url: string = "";

  // Additional Properties
  glMap?: HTMLGlMapElement;
  app?: HTMLGlAppElement;
  tabs?: HTMLIonTabsElement;
  drawer?: HTMLGlDrawerElement;
  buildingTiles =
    "https://maps.ccrpc.org/socio-economic-impact-tool/socio-economic-impact-tool.json";
  bufferChanged = false;

  @Listen("glDrawEnter")
  drawHasStarted(): void {
    this.drawing = true;
  }

  @Listen("glDrawExit")
  drawHasEnded(): void {
    this.drawing = false;
  }

  @Listen("glClickedFeaturesGathered", { target: "body" })
  async handleClick(e: CustomEvent): Promise<void> {
    if (e.detail.features.length === 0) {
      return;
    }

    const feature = e.detail.features[0];
    if (feature.sourceLayer === "segment") {
      const roadwayElem = feature.properties;
      const roadwayElemGeom = feature.geometry;
      const combinedElem = { ...roadwayElem, geometry: roadwayElemGeom };

      // Check if the segment is already in the selectedSegments array
      const segmentIndex = this.selectedSegments.findIndex(
        (seg) => seg.segment_id === combinedElem.segment_id
      );

      if (segmentIndex === -1) {
        // Add to selected segments if not already present
        this.selectedSegments = [...this.selectedSegments, combinedElem];
      } else {
        // Remove from selected segments if already present
        this.selectedSegments = this.selectedSegments.filter(
          (seg) => seg.segment_id !== combinedElem.segment_id
        );
      }
    } else if (feature.source === "drawn:drawn-features") {
      const idsToRemove: (string | number)[] = [];

      for (const feature of e.detail.features) {
        idsToRemove.push(feature.properties.id);
      }

      this.drawnFeatures = this.drawnFeatures.filter(
        (feature) => !idsToRemove.includes(feature.properties.id)
      );
    } else {
      throw new Error(
        `Cannot read feature type "${e.detail.features[0].source}"`
      );
    }
  }

  // Drawing Methods
  async initiateDraw(shape: "point" | "line" | "polygon"): Promise<void> {
    await this.startDraw(shape);
  }

  async startDraw(shape: "point" | "line" | "polygon"): Promise<void> {
    const drawController = this.el.querySelector("gl-draw-controller");
    const drawOptions: DrawOptions = {
      mapId: "mainMap",
      type: shape,
      toolbarLabel: `Draw New ${
        shape.charAt(0).toUpperCase() + shape.slice(1)
      }`,
      multiple: false,
    };

    // Start drawing and wait for it to complete
    const createdFeatures = await drawController.create(null, drawOptions);

    // Store the drawn features in the drawnFeatures array if any were created
    if (createdFeatures && createdFeatures.features.length > 0) {
      // We Save the id in properties so we will be able to identify this feature later (glFeaturesClickedGathered does not persist the id otherwise)
      createdFeatures.features.forEach((feature: Feature) => {
        feature.properties = {
          id: feature.id,
        };
      });

      this.drawnFeatures = [...this.drawnFeatures, ...createdFeatures.features];
    }
  }

  // Reset drawn features
  resetDraw(): void {
    this.drawnFeatures = [];
    alert("Drawn features have been reset.");
  }

  // Reset selected segments
  resetSegments(): void {
    this.selectedSegments = [];
    alert("Selected segments have been reset.");
  }

  @Watch("selectedSegments")
  selectedSegmentsChanged() {
    this.bufferChanged = true;
  }

  @Watch("drawnFeatures")
  drawnFeaturesChanged() {
    this.bufferChanged = true;
  }

  @Listen("bufferRendered")
  async handleIntersectingBuildingsChanged(
    e: CustomEvent<Feature<Polygon | MultiPolygon>>
  ): Promise<void> {
    if (!this.bufferChanged) {
      return;
    }

    const buffer = e.detail;

    // Find the intersecting features and the map screenshot
    const {
      filteredFeatures,
      map_area_screenshot,
    } = await findIntersectingFeatures(
      this.glMap.map,
      ["building:building-fill"],
      buffer
    );

    // Update the intersecting buildings and the map screenshot
    this.intersectingBuildingsWithUnion = filteredFeatures;
    this.map_screenshot_url = map_area_screenshot;

    this.bufferChanged = false;
  }

  @Listen("bufferAreaCalculated")
  handleBufferAreaCalculated(e: CustomEvent<string>): void {
    this.bufferArea = e.detail;
    console.log("Buffer area calculated: at root component ", this.bufferArea);
  }

  resetBuffer(resetInputValue = true) {
    if (resetInputValue) {
      const bufferDistanceInput = document.getElementById(
        "buffer-distance"
      ) as HTMLInputElement;
      bufferDistanceInput.value = "100";
    }

    this.displayBuffer = false;
    this.bufferChanged = true;
    alert("Buffer has been reset.");
  }

  removeDrawnFeatureLayers() {
    this.drawnFeatures = [];
  }

  render() {
    return (
      <gl-app
        label="Socio-economic Impact Tool"
        menu={true}
        menuLabel="Select Study Area"
        id="app-root"
      >
        <gl-draw-controller></gl-draw-controller>

        {/* End Slot Buttons */}
        <basemap-switcher slot="end-buttons"></basemap-switcher>
        <help-button slot="end-buttons"></help-button>
        <gl-fullscreen slot="end-buttons"></gl-fullscreen>
        <gl-share-button slot="end-buttons"></gl-share-button>

        {this.renderInfoPane()}
        {this.renderMapPane()}
        <gl-draw-toolbar slot="after-content" mapId="mainMap"></gl-draw-toolbar>
      </gl-app>
    );
  }

  renderInfoPane() {
    return (
      <div class="info-pane" slot="menu">
        {/* Step 1: Select Source */}
        <div class="step-container">
          <h2 class="step-title">Step 1: Select Source Type</h2>
          <div class="source-selection">
            <label htmlFor="source-dropdown">Select Source: </label>
            <select
              id="source-dropdown"
              class="buffer-select"
              onInput={(e) =>
                this.handleSourceSelection(
                  (e.target as HTMLSelectElement).value
                )
              }
            >
              <option value="road-segments" selected>
                From Road Segments
              </option>
              <option value="sketch">Sketch on the Map</option>
            </select>
          </div>
        </div>

        <div class="step-container">
          <h2 class="step-title">Step 2: Select Study Area</h2>
          <div id="source-message" class="source-message">
            {this.selectedSource === "road-segments" && (
              <div>
                <p>Select the road segments you want to choose on the map.</p>
                <ion-button
                  onClick={() => this.resetSegments()}
                  disabled={this.drawing}
                >
                  Reset Segments
                </ion-button>
              </div>
            )}
            {this.selectedSource === "sketch" && (
              <div class="sketch-options">
                <p>Select a sketch type:</p>
                <ion-button
                  onClick={() => this.initiateDraw("point")}
                  disabled={this.drawing}
                >
                  Draw Point
                </ion-button>
                <ion-button
                  onClick={() => this.initiateDraw("line")}
                  disabled={this.drawing}
                >
                  Draw Line
                </ion-button>
                <ion-button
                  onClick={() => this.initiateDraw("polygon")}
                  disabled={this.drawing}
                >
                  Draw Polygon
                </ion-button>
                <ion-button
                  onClick={() => this.resetDraw()}
                  disabled={this.drawing}
                >
                  Reset Drawings
                </ion-button>
              </div>
            )}
          </div>
        </div>

        {/* Step 3: Draw Buffer */}
        <div class="step-container">
          <h2 class="step-title">Step 3: Display Buffer</h2>
          {this.renderBufferControls()}
        </div>

        {/* Step 4: Generate Report */}
        <div class="step-container">
          <h2 class="step-title">Step 4: Generate report</h2>
          <report-generator
            intersectingBuildingsWithUnion={this.intersectingBuildingsWithUnion}
            bufferArea={this.bufferArea}
            study_area_url={this.map_screenshot_url}
          ></report-generator>
        </div>

        {/* Action Buttons */}
        <div class="action-bar">{this.renderActionButtons()}</div>
      </div>
    );
  }

  renderActionButtons() {
    return (
      <div style={{ textAlign: "center" }}>
        <div>
          <ion-button onClick={() => this.resetAll()}>
            <i class="icon">🔄</i>
            <span>Reset all selections</span>
          </ion-button>
        </div>
      </div>
    );
  }

  doDisplayBuffer = () => {
    this.displayBuffer = true;
  };

  handleBufferDistanceChange = (e: Event) => {
    const inputElement = e.target as HTMLInputElement;
    this.bufferSize = parseFloat(inputElement.value);
    this.bufferChanged = true;
  };

  handleBufferUnitChange = (e: Event) => {
    const selectElement = e.target as HTMLSelectElement;
    this.bufferUnit = selectElement.value as Units;
    this.bufferChanged = true;
  };

  renderBufferControls() {
    return (
      <div class="buffer-controls">
        <label htmlFor="buffer-distance">Buffer size:</label>
        <input
          type="number"
          id="buffer-distance"
          min="0"
          max="9999"
          defaultValue="100"
          class="buffer-input"
          onChange={this.handleBufferDistanceChange}
        />
        <label htmlFor="unit">Unit:</label>
        <select
          id="unit"
          class="buffer-select"
          onChange={this.handleBufferUnitChange}
        >
          <option value="meters">Meters</option>
          <option value="miles">Miles</option>
          <option value="kilometers">Kilometers</option>
          <option value="feet">Feet</option>
        </select>
        <div class="buffer-buttons">
          <ion-button class="buffer-button" onClick={this.doDisplayBuffer}>
            Draw Buffer
          </ion-button>
          <ion-button class="buffer-button" onClick={() => this.resetBuffer()}>
            Reset Buffer
          </ion-button>
        </div>
      </div>
    );
  }

  // New function to handle source selection
  handleSourceSelection(source: string) {
    this.selectedSource = source; // Update the selected source
  }

  renderMapPane() {
    const selectedSegments = this.selectedSegments.map(
      (segment) => segment.segment_id
    );

    const featuresToBuffer: Feature[] = [
      ...this.drawnFeatures,
      ...this.selectedSegments,
    ];

    return (
      <gl-map
        latitude={this.lat}
        longitude={this.long}
        hash={true}
        ref={(r: HTMLGlMapElement) => (this.glMap = r)}
        zoom={this.zoom}
        id="mainMap"
      >
        <app-buffer
          bufferDistance={this.bufferSize}
          bufferUnits={this.bufferUnit}
          featuresToBuffer={featuresToBuffer}
          doDisplayBuffer={this.displayBuffer}
        ></app-buffer>

        <gl-address-search
          url="https://geocode.ccrpc.org/search.php"
          bbox="-88.462126,39.879098,-87.931727,40.399428"
        ></gl-address-search>

        <drawn-features features={this.drawnFeatures}></drawn-features>

        <style-component
          segment-tiles={this.segmentTiles}
          building-tiles={this.buildingTiles}
          insertIntoStyle="basemap"
          selectedFeatureIds={
            selectedSegments.length !== 0 ? selectedSegments : [0]
          }
          disableClick={this.drawing}
        ></style-component>

        <gl-style
          url="https://maps.ccrpc.org/basemaps/hybrid-2023/style.json"
          basemap={true}
          thumbnail="https://maps.ccrpc.org/basemaps/hybrid/preview.jpg"
          name="2023 Aerial Imagery"
          enabled={false}
        ></gl-style>
        <gl-style
          url="https://maps.ccrpc.org/basemaps/dark/style.json"
          basemap={true}
          name="Dark"
          enabled={false}
        ></gl-style>
        <gl-style
          url="https://maps.ccrpc.org/basemaps/light/style.json"
          basemap={true}
          name="Light"
          enabled={false}
        ></gl-style>
        <gl-style
          url="https://maps.ccrpc.org/basemaps/basic/style.json"
          basemap={true}
          thumbnail="https://maps.ccrpc.org/basemaps/basic/preview.jpg"
          name="Basic"
          enabled={true}
        ></gl-style>
      </gl-map>
    );
  }

  resetAll() {
    this.resetDraw();
    this.removeDrawnFeatureLayers();
    this.resetBuffer(true);
    this.resetSegments();
    alert("All selections have been reset.");
  }
}
