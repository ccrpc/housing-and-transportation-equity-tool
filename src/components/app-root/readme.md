# app-root

<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type     | Default                                                    |
| -------------- | --------------- | ----------- | -------- | ---------------------------------------------------------- |
| `lat`          | `lat`           |             | `number` | `40.1207`                                                  |
| `long`         | `long`          |             | `number` | `-88.2337`                                                 |
| `segmentTiles` | `segment-tiles` |             | `string` | `"https://maps.ccrpc.org/access-score/baseline-2020.json"` |
| `zoom`         | `zoom`          |             | `number` | `10.96`                                                    |


## Dependencies

### Depends on

- gl-basemap-switcher
- gl-app
- gl-fullscreen
- gl-share-button
- ion-button
- [report-generator](../report)
- gl-draw-controller
- gl-draw-toolbar
- gl-map
- [app-buffer](../app-buffer)
- gl-address-search
- [drawn-features](../drawn-features)
- [style-component](../style-component)
- gl-style

### Graph
```mermaid
graph TD;
  app-root --> gl-basemap-switcher
  app-root --> gl-app
  app-root --> gl-fullscreen
  app-root --> gl-share-button
  app-root --> ion-button
  app-root --> report-generator
  app-root --> gl-draw-controller
  app-root --> gl-draw-toolbar
  app-root --> gl-map
  app-root --> app-buffer
  app-root --> gl-address-search
  app-root --> drawn-features
  app-root --> style-component
  app-root --> gl-style
  gl-basemap-switcher --> ion-item
  gl-basemap-switcher --> ion-thumbnail
  gl-basemap-switcher --> ion-radio
  gl-basemap-switcher --> ion-label
  gl-basemap-switcher --> ion-content
  gl-basemap-switcher --> ion-list
  gl-basemap-switcher --> ion-radio-group
  gl-basemap-switcher --> ion-list-header
  ion-item --> ion-icon
  ion-item --> ion-ripple-effect
  ion-item --> ion-note
  gl-app --> ion-menu
  gl-app --> ion-header
  gl-app --> ion-toolbar
  gl-app --> ion-title
  gl-app --> ion-content
  gl-app --> ion-menu-toggle
  gl-app --> ion-button
  gl-app --> ion-icon
  gl-app --> ion-buttons
  gl-app --> ion-footer
  gl-app --> ion-split-pane
  gl-app --> ion-app
  ion-menu --> ion-backdrop
  ion-button --> ion-ripple-effect
  gl-fullscreen --> ion-button
  gl-fullscreen --> ion-icon
  gl-share-button --> gl-share
  gl-share-button --> ion-button
  gl-share-button --> ion-icon
  gl-share --> ion-item
  gl-share --> ion-icon
  gl-share --> ion-label
  gl-share --> ion-list
  gl-share --> ion-list-header
  report-generator --> ion-button
  gl-draw-toolbar --> ion-toolbar
  gl-draw-toolbar --> ion-title
  gl-draw-toolbar --> ion-buttons
  gl-draw-toolbar --> ion-button
  gl-draw-toolbar --> ion-icon
  app-buffer --> gl-style
  gl-address-search --> ion-item
  gl-address-search --> ion-icon
  gl-address-search --> ion-label
  gl-address-search --> ion-list
  gl-address-search --> ion-searchbar
  ion-searchbar --> ion-icon
  drawn-features --> gl-style
  style-component --> gl-style
  style app-root fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
