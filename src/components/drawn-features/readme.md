# drawn-features

A child component to hold and render the features the users have added to the map

<!-- Auto Generated Below -->


## Overview

A child component representing the features that the user has manually added to the map.

## Properties

| Property   | Attribute | Description                                     | Type                                            | Default |
| ---------- | --------- | ----------------------------------------------- | ----------------------------------------------- | ------- |
| `features` | --        | Features the user has manually added to the map | `Feature<Geometry, { [name: string]: any; }>[]` | `[]`    |


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- gl-style

### Graph
```mermaid
graph TD;
  drawn-features --> gl-style
  app-root --> drawn-features
  style drawn-features fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
