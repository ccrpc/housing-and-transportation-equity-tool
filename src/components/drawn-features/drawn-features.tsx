import { Component, Host, Prop, h } from '@stencil/core';
import { StyleSpecification, LayerSpecification } from 'maplibre-gl';
import { Feature } from '@ccrpc/webmapgl';
import * as turf from '@turf/turf';

/* The styles for the drawn objects */
const DrawnFeaturesStyle: Array<LayerSpecification> = [
  {
    id: 'drawn-points',
    type: 'circle',
    source: 'drawn-features',
    paint: {
      'circle-radius': 3,
      'circle-color': 'red',
    },
  },
  {
    id: 'drawn-lines',
    type: 'line',
    source: 'drawn-features',
    paint: {
      'line-color': 'red',
      'line-width': 3,
    },
  },
];

/**
 * A child component representing the features that the user has manually added to the map.
 */
@Component({
  tag: 'drawn-features',
  styleUrl: 'drawn-features.css',
  shadow: false,
})
export class DrawnFeatures {
  /** Features the user has manually added to the map */
  @Prop() features: Feature[] = [];

  render() {
    if (this.features.length === 0) {
      return null;
    }

    const json: StyleSpecification = {
      version: 8,
      sources: {
        'drawn-features': {
          type: 'geojson',
          data: turf.featureCollection(this.features),
        },
      },
      layers: DrawnFeaturesStyle,
    };

    return (
      <Host>
        <gl-style name="drawn-features" id="drawn" basemap={false} json={json} enabled={true} clickableLayers={['drawn-points', 'drawn-lines']}></gl-style>
      </Host>
    );
  }
}
