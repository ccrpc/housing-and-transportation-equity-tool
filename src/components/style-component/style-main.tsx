import { Component, Prop, h } from "@stencil/core";

import baseTheme from "./assets/layers.json";
import socioEconomicToolBuildingTheme from "./assets/socioEconomicToolBuildingLayers.json";

@Component({
  tag: "style-component",
  styleUrl: "style-component.css",
})
export class StyleComponent {
  // Pass-through arguments
  @Prop() insertIntoStyle: string;
  @Prop() insertBeneathLayer: string;

  // Tile definitions
  @Prop() segmentTiles: string;

  @Prop() buildingTiles: string;

  // Layer Presentation options
  @Prop() theme = baseTheme;
  // Layer Presentation options
  @Prop() buildingTheme = socioEconomicToolBuildingTheme;

  @Prop() selectedFeatureIds: number[];

  /** Whether or not the user should be able to click on the segments */
  @Prop() disableClick: boolean = false;

  componentWillRender() {
    // Set Segment conditional styling
    this.theme.find(
      (thm) =>
        thm["source-layer"] === "segment" &&
        thm["id"] === "theme-custom-segments"
    ).paint["line-color"] = ([
      "case",
      ["in", ["get", "segment_id"], ["literal", this.selectedFeatureIds]],
      "#ffff00",
      "#000000",
    ] as unknown) as string;
  }

  render() {
    const appJson = {
      version: 8,
      sources: {
        segments: {
          type: "vector",
          url: this.segmentTiles,
        },
      },
      layers: this.theme,
      glyphs: "https://maps.ccrpc.org/fonts/{fontstack}/{range}.pbf",
    };

    const buildingJson = {
      version: 8,
      sources: {
        socio_economic_impact_tool_source: {
          type: "vector",
          url: this.buildingTiles,
          minzoom: 12,
        },
      },
      layers: this.buildingTheme,
      glyphs: "https://maps.ccrpc.org/fonts/{fontstack}/{range}.pbf",
    };

    return (
      <div>
        <gl-style
          id="app"
          insertIntoStyle={this.insertIntoStyle}
          insertBeneathLayer={this.insertBeneathLayer}
          json={appJson}
          clickableLayers={
            this.disableClick
              ? []
              : ["theme-custom-segments", "theme-custom-intersections"]
          }
        ></gl-style>
        <gl-style
          id="building"
          name="Buildings"
          json={buildingJson}
          insertIntoStyle={this.insertIntoStyle}
          insertBeneathLayer={this.insertBeneathLayer}
          basemap={false}
        ></gl-style>
      </div>
    );
  }
}
