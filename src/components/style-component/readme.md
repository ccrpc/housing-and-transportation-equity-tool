# style-component



<!-- Auto Generated Below -->


## Properties

| Property             | Attribute              | Description                                                     | Type                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                    | Default                   |
| -------------------- | ---------------------- | --------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------- |
| `buildingTheme`      | --                     |                                                                 | `({ id: string; source: string; "source-layer": string; type: string; paint: { "fill-color": string; "line-color"?: undefined; "line-width"?: undefined; }; layout: { "fill-sort-key": number; "line-sort-key"?: undefined; }; } \| { id: string; source: string; "source-layer": string; type: string; paint: { "line-color": string; "line-width": number; "fill-color"?: undefined; }; layout: { "line-sort-key": number; "fill-sort-key"?: undefined; }; })[]`                                                                                                      | `equityToolBuildingTheme` |
| `buildingTiles`      | `building-tiles`       |                                                                 | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | `undefined`               |
| `disableClick`       | `disable-click`        | Whether or not the user should be able to click on the segments | `boolean`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               | `false`                   |
| `insertBeneathLayer` | `insert-beneath-layer` |                                                                 | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | `undefined`               |
| `insertIntoStyle`    | `insert-into-style`    |                                                                 | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | `undefined`               |
| `segmentTiles`       | `segment-tiles`        |                                                                 | `string`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                | `undefined`               |
| `selectedFeatureIds` | --                     |                                                                 | `number[]`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              | `undefined`               |
| `theme`              | --                     |                                                                 | `({ id: string; type: string; source: string; "source-layer": string; minzoom: number; maxzoom: number; paint: { "line-color": any; "line-width": (string \| number \| (string \| number)[])[]; "line-offset"?: undefined; }; filter?: undefined; } \| { id: string; type: string; source: string; "source-layer": string; minzoom: number; maxzoom: number; filter: (string \| number \| string[])[]; paint: { "line-color": string; "line-width": (string \| number \| (string \| number)[])[]; "line-offset": (string \| number \| (string \| number)[])[]; }; })[]` | `baseTheme`               |


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- gl-style

### Graph
```mermaid
graph TD;
  style-component --> gl-style
  app-root --> style-component
  style style-component fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
