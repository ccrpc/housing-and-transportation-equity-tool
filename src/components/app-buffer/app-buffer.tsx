import { Component, Host, h, Prop, Event, EventEmitter } from "@stencil/core";
import { Feature } from "@ccrpc/webmapgl";
import {
  Map,
  GeoJSONFeature,
  LayerSpecification,
  StyleSpecification,
} from "maplibre-gl";
import {
  Point,
  LineString,
  Polygon,
  MultiPoint,
  MultiLineString,
  MultiPolygon,
} from "geojson";
import { buffer, featureCollection, union, area } from "@turf/turf";

/* The styles for the buffer */
const bufferStyle: Array<LayerSpecification> = [
  {
    id: "combined-buffers-fill",
    type: "fill",
    source: "combined-buffers",
    paint: {
      "fill-color": "blue",
      "fill-opacity": 0.3,
    },
  },
  {
    id: "combined-buffers-outline",
    type: "line",
    source: "combined-buffers",
    paint: {
      "line-color": "#000",
      "line-width": 1,
    },
  },
];

type Geometry =
  | Point
  | LineString
  | Polygon
  | MultiPoint
  | MultiLineString
  | MultiPolygon;

/**
 * A child component representing the buffer that is created around all of the features.
 */
@Component({
  tag: "app-buffer",
  styleUrl: "app-buffer.css",
  shadow: false,
})
export class AppBuffer {
  /** The distance from the center of the segments that the buffer will reach */
  @Prop() bufferDistance: number;

  /** In what unit the buffer distance is expressed */
  @Prop() bufferUnits: "miles" | "kilometers" | "feet" | "meters";

  /** A list of features around which to create the buffer */
  @Prop() featuresToBuffer: (Feature | GeoJSONFeature)[] = [];

  /** Whether or not to show the buffer */
  @Prop() doDisplayBuffer: boolean = false;

  @Event() bufferRendered: EventEmitter<Feature<Polygon | MultiPolygon>>;
  @Event() bufferAreaCalculated: EventEmitter<string>;

  combinedBuffer: Feature<Polygon | MultiPolygon>;
  bufferArea: string;

  render() {
    if (this.featuresToBuffer.length === 0 || !this.doDisplayBuffer) {
      return;
    }

    const geometries: Geometry[] = this.featuresToBuffer.map(
      (feature) => feature.geometry as Geometry
    );

    // Remove the last point if the first and last points are the same
    // This is necessary for the buffer to be created correctly.
    // Otherwise the enclosed part of the buffer has these polygonal holes.
    geometries.forEach((geometry) => {
      if (geometry.type === "LineString") {
        const coords = geometry.coordinates;
        if (
          coords[0][0] === coords[coords.length - 1][0] &&
          coords[0][1] === coords[coords.length - 1][1]
        ) {
          coords.pop();
        }
      }
    });

    const bufferFeatures = geometries.map((geometry) =>
      buffer(geometry, this.bufferDistance, { units: this.bufferUnits })
    );

    this.combinedBuffer =
      bufferFeatures.length === 1
        ? bufferFeatures[0]
        : union(featureCollection(bufferFeatures));

    // Calculate the area of the combined buffer in square meters
    const bufferAreaSqMeters = area(this.combinedBuffer);

    // If we want to always show the area in miles, keep this
    // Convert square meters to square miles (1 square meter = 3.861e-7 square miles)
    this.bufferArea = `${(bufferAreaSqMeters * 3.861e-7).toFixed(2)} sq miles`;

    // If we want to convert the area to the user's chosen unit, use this:
    // Convert the area to the chosen units
    // if (this.bufferUnits === "miles") {
    //   // Convert square meters to square miles (1 square meter = 3.861e-7 square miles)
    //   this.bufferArea = `${(bufferAreaSqMeters * 3.861e-7).toFixed(
    //     2
    //   )} sq miles`;
    // } else if (this.bufferUnits === "kilometers") {
    //   // Convert square meters to square kilometers (1 square meter = 1e-6 square kilometers)
    //   this.bufferArea = `${(bufferAreaSqMeters * 1e-6).toFixed(2)} sq km`;
    // } else if (this.bufferUnits === "feet") {
    //   // Convert square meters to square feet (1 square meter = 10.7639 square feet)
    //   this.bufferArea = `${(bufferAreaSqMeters * 10.7639).toFixed(2)} sq feet`;
    // } else {
    //   // Default to square meters
    //   this.bufferArea = `${bufferAreaSqMeters.toFixed(2)} sq metres`;
    // }

    const json: StyleSpecification = {
      version: 8,
      sources: {
        "combined-buffers": {
          type: "geojson",
          data: featureCollection([this.combinedBuffer]),
        },
      },
      layers: bufferStyle,
    };

    return (
      <Host>
        <gl-style
          name="buffer"
          id="buffer"
          basemap={false}
          json={json}
          enabled={true}
        ></gl-style>
      </Host>
    );
  }

  async componentDidRender() {
    if (this.featuresToBuffer.length === 0 || !this.doDisplayBuffer) {
      return;
    }

    this.bufferRendered.emit(this.combinedBuffer);

    // Emit the calculated buffer area
    this.bufferAreaCalculated.emit(this.bufferArea);
  }
}
