# app-buffer



<!-- Auto Generated Below -->


## Overview

A child component representing the buffer that is created around all of the features.

## Properties

| Property           | Attribute           | Description                                                             | Type                                                                | Default     |
| ------------------ | ------------------- | ----------------------------------------------------------------------- | ------------------------------------------------------------------- | ----------- |
| `bufferDistance`   | `buffer-distance`   | The distance from the center of the segments that the buffer will reach | `number`                                                            | `undefined` |
| `bufferUnits`      | `buffer-units`      | In what unit the buffer distance is expressed                           | `"feet" \| "kilometers" \| "meters" \| "miles"`                     | `undefined` |
| `doDisplayBuffer`  | `do-display-buffer` | Whether or not to show the buffer                                       | `boolean`                                                           | `false`     |
| `featuresToBuffer` | --                  | A list of features around which to create the buffer                    | `(GeoJSONFeature \| Feature<Geometry, { [name: string]: any; }>)[]` | `[]`        |


## Events

| Event                  | Description | Type                                                                      |
| ---------------------- | ----------- | ------------------------------------------------------------------------- |
| `bufferAreaCalculated` |             | `CustomEvent<string>`                                                     |
| `bufferRendered`       |             | `CustomEvent<Feature<Polygon \| MultiPolygon, { [name: string]: any; }>>` |


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- gl-style

### Graph
```mermaid
graph TD;
  app-buffer --> gl-style
  app-root --> app-buffer
  style app-buffer fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
