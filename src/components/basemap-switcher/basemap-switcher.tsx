import { h, Component, } from '@stencil/core';
import { popoverController } from "@ionic/core";


@Component({
  tag: 'basemap-switcher'
})
export class BasemapSwitcher {

    async openPopoverForBasemaps(ev: UIEvent) {
        const options = {
          component: document.createElement("gl-basemap-switcher"),
          ev: ev,
        };
        const popover = await popoverController.create(options);
        await popover.present();
        return popover;
    }
    
    selectBasemap = (e: UIEvent) => this.openPopoverForBasemaps(e);
      

  render() {
    return (
      <ion-button onClick={this.selectBasemap}
          title="Change Basemap">
        <ion-icon slot="icon-only" name="map-outline"></ion-icon>
      </ion-button>
    );

    
  }
}
