import { h, Component, } from '@stencil/core';
import { modalController } from "@ionic/core";


@Component({
  tag: 'help-button'
})
export class HelpButton {

  // Modal and Popover Methods
    async openModal(component: string) {
      const modal = await modalController.create({
        component: component,
      });
      await modal.present();
    }

  render() {
    return (
      <ion-button onClick={(e) => this.openModal("help-content")}
          title="Help">
        <ion-icon slot="icon-only" name="help-outline"></ion-icon>
      </ion-button>
    );
  }
}
