/**
 * Generates a PDF report using the jsPDF library based on intersecting buildings data.
 *
 * The report includes tables with calculated totals and percentages for various metrics.
 *
 * Preconditions:
 * - `this.intersectingBuildingsWithUnion` must be defined and contain building data.
 * - The length of `this.intersectingBuildingsWithUnion` must be at least 10.
 *
 * If the preconditions are not met, appropriate messages are logged and alerts are shown.
 *
 * The report includes:
 * - A title "Socio-economic Impact Tool Report".
 * - Tables for each set of keys and labels defined in `labelsAndKeys`.
 *
 * Each table includes:
 * - A title.
 * - Calculated totals for each key.
 * - Percentages of each total relative to the first total.
 * - Values less than 5 are displayed as "less than 5".
 *
 * The generated PDF is opened in a new browser window.
 */

import { Component, Prop, h } from "@stencil/core";
import { jsPDF } from "jspdf";
import autoTable, { CellDef } from "jspdf-autotable";
import labelsAndKeys from "./labels-and-keys.json";

@Component({
  tag: "report-generator",
  styleUrl: "report-generator.css",
  shadow: false,
})
export class ReportGenerator {
  @Prop() intersectingBuildingsWithUnion: any[];
  @Prop() bufferArea: string;
  @Prop() study_area_url: string;

  // Generate the report based on the intersecting buildings data
  // Main function that then calls other functions to generate the report
  generateReport() {
    // Check if there are intersecting buildings to generate a report
    if (
      !this.intersectingBuildingsWithUnion ||
      this.intersectingBuildingsWithUnion.length === 0
    ) {
      console.log("No intersecting buildings to generate a report.");
      alert("No intersecting buildings to generate a report.");
      return;
    }

    // Check if the sample size is large enough to generate a report
    if (this.intersectingBuildingsWithUnion.length < 10) {
      console.log("Sample size is too small, please select a larger area.");
      alert("Sample size is too small, please select a larger area.");
      return;
    }

    console.log("Generating report...");

    //If both conditions are met, generate the report

    // Create a new PDF document
    const doc = new jsPDF();

    // Get the page width
    const pageWidth = doc.internal.pageSize.width;
    const pageHeight = doc.internal.pageSize.height;

    // Page 1
    doc.setFontSize(20);
    // Rectangle dimensions
    const rectX = 5; // X-coordinate of the rectangle
    const rectY = 15; // Y-coordinate of the rectangle

    // Calculate the rectangle width to be as wide as the page with 10 on each side as margin
    const rectWidth = pageWidth - 10; // Page width minus 10 cuz it starts at 5, so other side is 5, total is 10
    const rectHeight = 60; // Height of the rectangle

    // Draw the rectangle with a black border
    doc.setLineWidth(0.5);
    doc.rect(rectX, rectY, rectWidth, rectHeight); // x, y, width, height

    let yOffset = 30; // Set the yOffset for the text; yOffset means height from the top of the page

    // Set font and alignment for text
    doc.setFont("helvetica", "bold");

    // Line 1
    doc.text("Socio-economic Impact Tool", pageWidth / 2, yOffset, {
      align: "center",
    });

    // Line 2
    doc.setFontSize(18);
    yOffset += 10;
    doc.text("Buffer Area Analysis Report", pageWidth / 2, yOffset, {
      align: "center",
    });

    // Line 3
    yOffset += 15;
    doc.setFontSize(12);
    doc.text(
      "Data Source: American Community Survey 2018-2022",
      pageWidth / 2,
      yOffset,
      { align: "center" }
    );

    // Line 4
    yOffset += 10;
    doc.setFontSize(12);
    const today = new Date().toLocaleDateString();
    doc.text(`Created On: ${today}`, pageWidth / 2, yOffset, {
      align: "center",
    });
    console.log("Adding study area image");
    console.log("Study Area Url: ", this.study_area_url);

    // Add the study area image if the URL is defined
    if (this.study_area_url) {
      const imageUrl = this.study_area_url;
      const horizontalStart = 10;
      const verticalStart = 90;
      const imageWidth = pageWidth - 20;
      const imageHeight = 120;

      // Add the study area image to the PDF
      // Module at: https://artskydj.github.io/jsPDF/docs/module-addImage.html
      // Format: addImage(imageData, format, x, y, width, height, alias, compression, rotation)
      doc.addImage(
        imageUrl,
        "PNG",
        horizontalStart,
        verticalStart,
        imageWidth,
        imageHeight,
        "study_area",
        "FAST"
      );
    }
    console.log("Adding statistics");

    doc.setFont("helvetica", "normal");

    // Define a function to add statistics to the first page of the report
    const addStatistics = (doc) => {
      const pageHeight = doc.internal.pageSize.height;
      const yOffset = pageHeight - 75;

      doc.setFontSize(12);
      autoTable(doc, {
        startY: yOffset,
        head: [["Metric", "Value"]],
        theme: "grid",
        margin: { left: 45, right: 45 },
        body: [
          ["Buffer Area", this.bufferArea],
          ["Disadvantaged Area", "0.0 square miles"],
          ["Percent Area Disadvantaged", "0.0%"],
          ["Estimated Disadvantaged Population", "0"],
        ],
        styles: {
          fontSize: 10,
        },
        headStyles: {
          halign: "center", // Center align the heading row
        },
        columnStyles: {
          0: { cellWidth: 80, halign: "left" },
          1: { cellWidth: 40, halign: "left" },
        },
      });

      doc.setFontSize(12);
    };

    addStatistics(doc);

    // Add a new page
    doc.addPage();

    // Page 2 and onwards
    doc.setFontSize(18);
    doc.setFont("helvetica", "bold");
    doc.text(
      `${this.bufferArea} Buffer Analysis Summary Report`,
      pageWidth / 2,
      20,
      {
        align: "center",
      }
    );

    doc.setFontSize(12);
    doc.text(
      "Due to rounding, counts may not add up exactly to 100%",
      pageWidth / 2,
      30,
      { align: "center" }
    );

    // Initialize the yOffset for the tables past the title
    yOffset = 40;

    // Calculate totals and percentages for each key and label

    /**
     * Flow of the function:
     * 1. `generateReport` is called when the user clicks the "Generate Report" button.
     * 2. Preconditions are checked:
     *    - If `intersectingBuildingsWithUnion` is not defined or empty, log a message and show an alert.
     *    - If the length of `intersectingBuildingsWithUnion` is less than 10, log a message and show an alert.
     * 3. A new PDF document is created using jsPDF.
     * 4. The title "Socio-economic Impact Tool Report" is added to the PDF.
     * 5. The yOffset for the tables is initialized.
     * 6. For each set of keys and labels in `labelsAndKeys`:
     *    - `generateReportTable` is called with the title, keys, and labels.
     *    - `generateReportTable` calls `calculateTotals` to calculate totals and percentages.
     *    - `calculateTotals`:
     *      - Initializes an empty array `formattedData`.
     *      - Calls `processKeysAndLabels` recursively to process nested keys and labels.
     *      - `processKeysAndLabels`:
     *        - For each key:
     *          - If the key is an array (sublist), process each subkey within the sublist:
     *            - Calculate the total for each subkey.
     *            - Format the label with increased indentation for sublist items.
     *            - Push the sublist data to `formattedData`.
     *          - If the key is a top-level key:
     *            - Calculate the total for the key.
     *            - Format the label with the appropriate indentation.
     *            - Push the top-level data to `formattedData`.
     *      - Calculate percentages for each item based on the total sums.
     *      - Return the formatted data with totals and percentages.
     *    - `generateTable` is called with the title and calculated data.
     *    - `generateTable` uses autoTable to generate the table in the PDF.
     * 7. The generated PDF is opened in a new browser window.
     */

    const calculateTotals = (
      keys: (string | string[])[],
      labels: (string | string[])[],
      indentLevel = 0
    ) => {
      const formattedData = [];

      // Recursive function to process nested keys and labels
      const processKeysAndLabels = (
        keys: (string | string[])[],
        labels: (string | string[])[],
        indentLevel: number
      ) => {
        keys.forEach((key, index) => {
          if (Array.isArray(key)) {
            // Handle sublist
            let sublistTotal = 0;

            key.forEach((subKey, subIndex) => {
              const isFirstInSublist = subIndex === 0;
              const labelIndent = indentLevel + (isFirstInSublist ? 1 : 2);

              const total = this.intersectingBuildingsWithUnion.reduce(
                (sum, building) =>
                  sum + parseFloat(building.properties[subKey] || 0),
                0
              );

              if (isFirstInSublist) sublistTotal = total; // Capture total for the first sublist item

              const labelWithSpaces = {
                content: `${"  ".repeat(labelIndent)}${
                  labels[index][subIndex]
                }`,
                styles: { fontStyle: "normal" },
              };

              // Push sublist data with both the grand total and sublist total
              formattedData.push([
                labelWithSpaces,
                total,
                sublistTotal,
                isFirstInSublist,
              ]);
            });
          } else {
            // Handle top-level key
            const total = this.intersectingBuildingsWithUnion.reduce(
              (sum, building) =>
                sum + parseFloat(building.properties[key] || 0),
              0
            );

            const isFirstInList = index === 0;
            const labelIndent = isFirstInList ? indentLevel : indentLevel + 1;

            const labelWithSpaces = {
              content: `${"  ".repeat(labelIndent)}${labels[index]}`,
              styles:
                isFirstInList && formattedData.length === 0
                  ? { fontStyle: "bold" }
                  : { fontStyle: "normal" },
            };

            // Top-level items only use the grand total
            formattedData.push([labelWithSpaces, total, null, false]);
          }
        });
      };

      // Start processing the keys and labels
      processKeysAndLabels(keys, labels, indentLevel);

      // Calculate percentages
      const totalSum = formattedData[0][1]; // Grand total is the first row
      const dataWithPercentages = formattedData.map(
        ([label, total, sublistTotal, isFirstInSublist]) => {
          // Determine percentage based on context
          const percentage =
            isFirstInSublist && sublistTotal !== null
              ? ((total / totalSum) * 100).toFixed(2) + "%" // Sublist's first item uses the grand total
              : sublistTotal
              ? ((total / sublistTotal) * 100).toFixed(2) + "%" // Sublist percentages
              : ((total / totalSum) * 100).toFixed(2) + "%"; // Top-level percentage

          // Display "less than 5" for values less than 5
          return [
            label,
            total > 0 && total < 5 ? "less than 5" : total,
            isNaN(parseFloat(percentage)) ? "-" : percentage,
          ];
        }
      );

      return dataWithPercentages;
    };

    const addPageNumber = (doc) => {
      const pageCount = doc.internal.getNumberOfPages();
      for (let i = 1; i <= pageCount; i++) {
        doc.setPage(i);
        doc.setFontSize(10);
        const pageWidth = doc.internal.pageSize.width;
        const pageHeight = doc.internal.pageSize.height;
        doc.text(`Page ${i} of ${pageCount}`, pageWidth / 2, pageHeight - 10, {
          align: "center",
        });

        doc.setFontSize(10);
        doc.setFont("helvetica", "italic");

        doc.text(
          "* The variables marked with an asterisk are estimated from Census Tract level data because the variables are not available",
          rectX,
          pageHeight - 20,
          {
            align: "left",
          }
        );

        doc.text("  at the Block Group level.", rectX, pageHeight - 15, {
          align: "left",
        });
      }
    };

    // Generate the population table after calculating totals and percentages
    const generateTable = (
      title: string | number | boolean | string[] | CellDef,
      rows: any[][]
    ) => {
      autoTable(doc, {
        startY: yOffset,
        head: [[title, "Buffer Estimates", "Percent"]],
        body: rows,
        margin: { bottom: 30 },
        columnStyles: {
          0: { cellWidth: 120 },
          1: { cellWidth: 40, halign: "center" },
          2: { cellWidth: 20, halign: "center" },
        },
        styles: {
          fontSize: 10,
        },
        didDrawPage: (data) => {
          yOffset = data.cursor.y + 10; // Update yOffset for the next table
        },
      });
    };

    // Generate the report table with the calculated totals and percentages
    const generateReportTable = (
      title: string,
      keys: (string | string[])[],
      labels: (string | string[])[]
    ) => {
      const totals = calculateTotals(keys, labels);
      generateTable(title, totals);
    };

    // Traverse through the list and generate the report tables
    for (const { title, keys, labels } of labelsAndKeys) {
      generateReportTable(title, keys, labels);
    }

    addPageNumber(doc);

    // Open the PDF in a new tab
    doc.output("dataurlnewwindow");
  }

  render() {
    return (
      <ion-button
        disabled={!this.study_area_url}
        onClick={() => this.generateReport()}
      >
        Generate Report
      </ion-button>
    );
  }
}
