# report-generator



<!-- Auto Generated Below -->


## Properties

| Property                         | Attribute        | Description | Type     | Default     |
| -------------------------------- | ---------------- | ----------- | -------- | ----------- |
| `bufferArea`                     | `buffer-area`    |             | `string` | `undefined` |
| `intersectingBuildingsWithUnion` | --               |             | `any[]`  | `undefined` |
| `study_area_url`                 | `study_area_url` |             | `string` | `undefined` |


## Dependencies

### Used by

 - [app-root](../app-root)

### Depends on

- ion-button

### Graph
```mermaid
graph TD;
  report-generator --> ion-button
  ion-button --> ion-ripple-effect
  app-root --> report-generator
  style report-generator fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
