# ht-modal



<!-- Auto Generated Below -->


## Properties

| Property | Attribute | Description | Type     | Default     |
| -------- | --------- | ----------- | -------- | ----------- |
| `name`   | `name`    |             | `string` | `undefined` |


## Dependencies

### Used by

 - [help-content](../help-content)

### Depends on

- ion-header
- ion-toolbar
- ion-title
- ion-buttons
- ion-button
- ion-icon
- ion-content

### Graph
```mermaid
graph TD;
  ht-modal --> ion-header
  ht-modal --> ion-toolbar
  ht-modal --> ion-title
  ht-modal --> ion-buttons
  ht-modal --> ion-button
  ht-modal --> ion-icon
  ht-modal --> ion-content
  ion-button --> ion-ripple-effect
  help-content --> ht-modal
  style ht-modal fill:#f9f,stroke:#333,stroke-width:4px
```

----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
