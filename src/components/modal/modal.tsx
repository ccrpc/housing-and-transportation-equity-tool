import { h, Component, Element, Prop } from "@stencil/core";

@Component({
  styleUrl: "modal.css",
  tag: "ht-modal",
})
export class Modal {
  @Element() el: HTMLElement;

  @Prop() name: string;

  closeModal() {
    this.el.closest("ion-modal").dismiss();
  }

  render() {
    return [
      <ion-header>
        <ion-toolbar>
          <ion-title>{this.name}</ion-title>
          <ion-buttons slot="end">
            <ion-button
              onClick={() => this.closeModal()}
              title={`Close ${this.name}`}
            >
              <ion-icon slot="icon-only" name="close"></ion-icon>
            </ion-button>
          </ion-buttons>
        </ion-toolbar>
      </ion-header>,
      <ion-content class="ion-padding">
        <slot />
      </ion-content>,
    ];
  }
}
