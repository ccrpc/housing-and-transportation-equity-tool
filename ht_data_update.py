import requests #This allows us to pull a link and scrape its data
import pandas as pd # Builds dataframes
import geopandas as gpd # Builds geodataframes
import os # Allows access and manipulation of file system
from dotenv import load_dotenv # To pull from .env file
from sqlalchemy import create_engine # To create Database connection

# This is the file that we will get our new labels from
df = pd.read_excel('/home/ani44987@co.champaign.il.us/Documents/scripts/python/Updated_Data_List_Equity_Tool.xlsx')

# Here we will be creating a dictionary of the variables and their appropriate titles so that we can quickly update all of them
# to be PostgreSQL compliant
replace_dict = {}

# This loops through all rows in the 4th column and applies our previous function to it.
# The column doesn't have a name so I identified it by number, on future files it may be different.
key = ''
variable = ''
for row in range(len(df)):
    if df.isna().iloc[row,5] == False and df.isna().iloc[row,12] == False:
        key = df.iloc[row,5].strip()
        variable = df.iloc[row,12].strip()
        replace_dict.update({key: variable})

# Make sure to add a file called .env that has the line CENSUS='{insert_census_api_key_here}'. You can also run `dotenv set CENSUS {insert_census_api_key_here}` in the terminal
load_dotenv()

print('pulling env...')
DB_HOST = os.environ.get("DB_HOST")
DB_NAME = os.environ.get("DB_NAME")
DB_USER = os.environ.get("DB_USER")
DB_PASSWORD = os.environ.get("DB_PASSWORD")
DB_PORT = os.environ.get("DB_PORT")
key = os.environ.get('CENSUS')

year = '2022'

# The Census API only accepts requests of 50 variables in a single call so our variables are chunked into 50
# These variables are organized by whether they are block group or tract geometry
# After that they are then secondarily grouped by which table they are sourced from (data profile/subject/detailed)
# Lastly they are identified by which chunk of 50 they are (group 1, 2 3, or 4 etc.)

# If you add new variables make sure to follow the convention "geography_table_variables_#"
block_detailed_1 = 'B01001_001E,B01001_002E,B01001_026E,B02001_001E,B02001_002E,B02001_003E,B02001_004E,B02001_005E,B02001_006E,B02001_007E,B02001_008E,B02001_009E,B02001_010E,B03002_001E,B03002_002E,B03002_003E,B03002_004E,B03002_005E,B03002_006E,B03002_007E,B03002_008E,B03002_009E,B03002_012E,B03002_013E,B03002_014E,B03002_015E,B03002_016E,B03002_017E,B03002_018E,B03002_019E,B11001_001E,B11001_002E,B11001_003E,B11001_004E,B11001_007E,B11001_008E,B11001_009E,B11016_001E,B11016_002E,B11016_003E,B11016_004E,B11016_005E,B11016_006E,B11016_007E,B11016_008E,B11016_009E'

block_detailed_2 = 'B11016_010E,B11016_011E,B11016_012E,B11016_013E,B11016_014E,B11016_015E,B11016_016E,B15003_001E,B15003_002E,B15003_003E,B15003_004E,B15003_005E,B15003_006E,B15003_007E,B15003_008E,B15003_009E,B15003_010E,B15003_011E,B15003_012E,B15003_013E,B15003_014E,B15003_015E,B15003_016E,B15003_017E,B15003_018E,B15003_019E,B15003_020E,B15003_021E,B15003_022E,B15003_023E,B15003_024E,B15003_025E,B19001_001E,B19001_002E,B19001_003E,B19001_004E,B19001_005E,B19001_006E,B19001_007E,B19001_008E,B19001_009E,B19001_010E,B19001_011E,B19001_012E,B19001_013E,B19001_014E'

block_detailed_3 = 'B19001_015E,B19001_016E,B19001_017E,B21001_001E,B21001_002E,B21001_003E,B23025_001E,B23025_002E,B23025_003E,B23025_006E,B23025_007E,B25070_001E,B25070_002E,B25070_003E,B25070_004E,B25070_005E,B25070_006E,B25070_007E,B25070_008E,B25070_009E,B25070_010E,B25070_011E,B25091_001E,B25091_002E,B25091_003E,B25091_004E,B25091_005E,B25091_006E,B25091_007E,B25091_008E,B25091_009E,B25091_010E,B25091_011E,B25091_012E,B25091_013E,B25091_014E,B25091_015E,B25091_016E,B25091_017E,B25091_018E,B25091_019E,B25091_020E,B25091_021E,B25091_022E,B25091_023E,B28001_001E,B28001_002E,B28001_003E,B28010_005E'

block_detailed_4 = 'B28010_007E,B28002_001E,B28002_002E,B28011_003E,B28011_004E,B28011_005E,B28011_006E,B28011_007E,B28011_008E,C16002_001E,C16002_002E,C16002_003E,C16002_004E,C16002_005E,C16002_006E,C16002_007E,C16002_008E,C16002_009E,C16002_010E,C16002_011E,C16002_012E,C16002_013E,C16002_014E'

tract_detailed_1 = 'B03001_001E,B03001_002E,B03001_003E,B03001_004E,B03001_005E,B03001_006E,B03001_007E,B03001_008E,B03001_016E,B03001_027E,B05012_001E,B05012_002E,B05012_003E,B17001_001E,B17001_002E,B17001_003E,B17001_017E,B17001_031E,B17001_032E,B17001_046E,B17020_001E,B17020_002E,B17020_003E,B17020_004E,B17020_005E,B17020_006E,B17020_007E,B17020_008E,B17020_009E,B17020_010E,B17020_011E,B17020_012E,B17020_013E,B17020_014E,B17020_015E,B17020_016E,B17020_017E,B17020A_001E,B17020B_001E,B17020C_001E,B17020D_001E,B17020E_001E,B17020F_001E,B17020G_001E,B18135_001E,B18135_002E'

tract_detailed_2 = 'B18135_003E,B18135_008E,B18135_013E,B18135_014E,B18135_019E,B18135_024E,B18135_025E,B18135_030E,B25104_001E,B25104_002E,B25104_003E,B25104_004E,B25104_005E,B25104_006E,B25104_007E,B25104_008E,B25104_009E,B25104_010E,B25104_011E,B25104_012E,B25104_013E,B25104_014E,B25104_015E,B25104_016E,B25104_017E'

tract_data_1 = 'DP05_0039E,DP05_0040E,DP05_0041E,DP05_0042E,DP05_0043E,DP04_0002E,DP04_0058E,DP04_0059E,DP04_0060E,DP04_0061E,DP02_0001E,DP02_0002E,DP02_0004E,DP02_0006E,DP02_0010E'

# Creating combined lists of all variables
block_list = [block_detailed_1,block_detailed_2,block_detailed_3,block_detailed_4]
tract_list = [tract_detailed_1,tract_detailed_2,tract_data_1]

# These are the Census API calls
# Each table and geometry has a unique API link
# Place the desired variable string in the correct link
# change the api link format depending on if variables need to be pulled from subject/detailed/or data profile
print('pulling blocks')
block_dfs = pd.DataFrame()
tract_dfs = pd.DataFrame()

# Loop through the list of block variables put them into a temporary dataframe and then combine them with the combined dataframe block_dfs
for variables in block_list:
  data = requests.get(f"https://api.census.gov/data/{year}/acs/acs5?get=NAME,{variables}&for=block%20group:*&in=state:17%20county:019&key={key}")
  data = data.json()
  temp_df = pd.DataFrame(data[1:], columns=data[0])
  temp_df['GEOID'] = temp_df[temp_df.columns[-4]] + temp_df[temp_df.columns[-3]] + temp_df[temp_df.columns[-2]] + temp_df[temp_df.columns[-1]]

  if block_dfs.empty is True:
    block_dfs = temp_df
  else:
    temp_df = temp_df.drop(columns=['tract', 'NAME','county','block group', 'state'])
    block_dfs = pd.merge(block_dfs, temp_df, on='GEOID', how='left')

print('pulling tracts')
# Loop through the list of tract variables put them into a temporary dataframe and then combine them with the combined dataframe tract_dfs
tract_dfs = pd.DataFrame()
for variables in tract_list:
    if 'B' in variables: # Detailed table variables start with B
        data = requests.get(f"https://api.census.gov/data/{year}/acs/acs5?get=NAME,{variables}&for=tract:*&in=state:17&in=county:019&key={key}")
        data = data.json()
        temp_df = pd.DataFrame(data[1:], columns=data[0])
        temp_df['GEOID'] = temp_df[temp_df.columns[-3]] + temp_df[temp_df.columns[-2]] + temp_df[temp_df.columns[-1]]
        if tract_dfs.empty is True:
            tract_dfs = temp_df
        else:
            temp_df = temp_df.drop(columns=['tract', 'NAME','county', 'state'])
            tract_dfs = pd.merge(tract_dfs, temp_df, on='GEOID', how='left')
    elif 'D' in variables:
        data = requests.get(f"https://api.census.gov/data/{year}/acs/acs5/profile?get={variables}&for=tract:*&in=state:17&in=county:019&key={key}")
        data = data.json()
        temp_df = pd.DataFrame(data[1:], columns=data[0])
        temp_df['GEOID'] = temp_df[temp_df.columns[-3]] + temp_df[temp_df.columns[-2]] + temp_df[temp_df.columns[-1]]
        if tract_dfs.empty is True:
            tract_dfs = temp_df
        else:
            temp_df = temp_df.drop(columns=['tract','county', 'state'])
            tract_dfs = pd.merge(tract_dfs, temp_df, on='GEOID', how='left')

print('renaming columns')
block_dfs.rename(columns = replace_dict, inplace=True)
tract_dfs.rename(columns = replace_dict, inplace=True)

# Geometries for census tracts and block groups
block_tiger_url = f"https://www2.census.gov/geo/tiger/TIGER{year}/BG/tl_{year}_17_bg.zip"
census_tiger_url = f"https://www2.census.gov/geo/tiger/TIGER{year}/TRACT/tl_{year}_17_tract.zip"

print('pulling geographies')
# Filter for just champaign county
block_geography = gpd.read_file(block_tiger_url)
block_geography = block_geography.drop(columns=['NAMELSAD']) # Remove geography naming convention
block_geography = block_geography[
    (block_geography['STATEFP'] == '17') &
    (block_geography['COUNTYFP'] == '019')
]

print('filtering geographies')
# Filter for just champaign county
tract_geography = gpd.read_file(census_tiger_url)
tract_geography = tract_geography.drop(columns=['NAMELSAD','NAME'])
tract_geography = tract_geography[
    (tract_geography['STATEFP'] == '17') &
    (tract_geography['COUNTYFP'] == '019')
]

print('setting crs...')
# Set to proper CRS
block_geography = block_geography.to_crs(epsg = 3435)
tract_geography = tract_geography.to_crs(epsg = 3435)

print('merging data and geographies...')
# Merge with combined dfs
block_group = block_geography.merge(block_dfs, on = "GEOID")
census_tract = tract_geography.merge(tract_dfs, on = "GEOID")

print("making columns lower case...")
block_group.rename(columns=lambda x: x.lower(), inplace=True)
census_tract.rename(columns=lambda x: x.lower(), inplace=True)

print('uploading to database')
# Upload straight to database
engine = create_engine(f"postgresql+psycopg2://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}")

census_tract.to_postgis("census_tract", engine, schema="equity", if_exists="replace")
block_group.to_postgis("block_group", engine, schema="equity", if_exists="replace")

# If you want to save the data to your local file system comment out lines 147 through 152 and uncomment the lines below

# Indicate the folder where all the csvs are stored from the above script
# file_path = <filepath>

# Turn the geodatagrams into geopackages and save them to your filesystem
# block_group.to_file(file_path + 'block_group.gpkg', driver='GPKG')
# census_tract.to_file(file_path + 'census_tract.gpkg', driver='GPKG')
