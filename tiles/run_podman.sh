#!/bin/bash

podman build -t lui.tiles:export .
podman run -v $PWD/output:/data:Z --env-file .env --name export_tiles lui.tiles:export
podman container rm export_tiles
podman image rm lui.tiles:export