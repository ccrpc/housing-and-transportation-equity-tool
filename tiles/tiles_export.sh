#!/bin/sh

# Data sources: PostGIS and GeoPackage
GPKG=/data/census_zcta.gpkg
MBTILES=/data/census_zcta.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

read -r -d '' census_zcta_query <<EOM
SELECT
  *, -- All fields from subquery as well as the percentiles below.
  (ntile(100) over (order by diverse_percentage) - 1)/100.0 as diverse_percentile,
  (ntile(100) over (order by disability_percentage) - 1)/100.0 as disability_percentile,
  (ntile(100) over (order by vet_percentage) - 1)/100.0 as vet_percentile,
  (ntile(100) over (order by iq_families_percentage) - 1)/100.0 as iq_families_percentile,
  (ntile(100) over (order by iq_seniors_percentage) - 1)/100.0 as iq_seniors_percentile,
  (ntile(100) over (order by seniors_percentage) - 1)/100.0 as seniors_percentile,
  (ntile(100) over (order by renters_percentage) - 1)/100.0 as renters_percentile,
  (ntile(100) over (order by unemployed_percentage) - 1)/100.0 as unemployed_percentile,
  (ntile(100) over (order by less_hs_diploma_percentage) - 1)/100.0 as less_hs_diploma_percentile,
  (ntile(100) over (order by asthma_percentage) - 1)/100.0 as asthma_percentile,
  (ntile(100) over (order by single_unit_households_percentage) - 1)/100.0 as single_unit_households_percentile,
  (ntile(100) over (order by multiple_unit_households_percentage) - 1)/100.0 as multiple_unit_households_percentile,
  (ntile(100) over (order by mobile_and_other_households_percentage) - 1)/100.0 as mobile_and_other_households_percentile,
  (ntile(100) over (order by limited_english_households_percentage) - 1)/100.0 as limited_english_households_percentile
FROM (
  SELECT DISTINCT
    ameren_zcta.individuals,
    ameren_zcta.households,
    ameren_zcta.family_households,
    ameren_zcta.zcta as geoid,
    ST_MakeValid(zcta.geom) as geom,
    1 - ameren_zcta.non_hispanic_white :: decimal / NULLIF(ameren_zcta.individuals, 0) AS diverse_percentage,
    ameren_zcta.disability_pop :: decimal / NULLIF(disability_noninstitutionalized_pop, 0) AS disability_percentage,
    ameren_zcta.veterans :: decimal / NULLIF(veterans_civilian_pop, 0) AS vet_percentage,
    ameren_zcta.family_households_income_0_299_pl :: decimal / NULLIF(family_households, 0) AS iq_families_percentage,
    ameren_zcta.pop_by_age_65_plus_0_299_pl :: decimal / NULLIF(pop_by_age_65_plus, 0) AS iq_seniors_percentage,
    ameren_zcta.pop_by_age_65_plus :: decimal / NULLIF(ameren_zcta.individuals, 0) AS seniors_percentage,
    ameren_zcta.renter_occupied_units :: decimal / NULLIF(occupied_housing_units, 0) AS renters_percentage,
    ameren_zcta.unemployed_civilian :: decimal / NULLIF(civilian_labor_force, 0) AS unemployed_percentage,
    ameren_zcta.pop_25_plus_less_than_hs :: decimal / NULLIF(pop_25_plus, 0) AS less_hs_diploma_percentage,
    test_asthma_zcta.data_value / 100.0 as asthma_percentage,
    ameren_zcta.single_unit_households :: decimal / NULLIF(households, 0) AS single_unit_households_percentage,
    ameren_zcta.multiple_unit_households :: decimal / NULLIF(households, 0) AS multiple_unit_households_percentage,
    ameren_zcta.mobile_and_other_households :: decimal / NULLIF(households, 0) AS mobile_and_other_households_percentage,
    ameren_zcta.limited_english_households_percentage
  FROM
    census.ameren_zcta

  INNER JOIN (
    SELECT geoid,geom
    FROM census.spatial
    WHERE level='ZCTA'
  ) as zcta
  ON CAST(ameren_zcta.zcta AS BIGINT) = CAST(zcta.geoid AS BIGINT)

  LEFT JOIN census.test_asthma_zcta ON CAST(ameren_zcta.zcta AS BIGINT) = CAST(test_asthma_zcta.zcta AS BIGINT)
  INNER JOIN (
    SELECT ST_Transform(ST_Union(geom),3435) as geom FROM census.service_boundary_test
  ) as boundary ON ST_Intersects(zcta.geom, boundary.geom)
) percentiles
EOM

read -r -d '' zcta_centroid_query <<EOM
SELECT DISTINCT
    zcta.geoid,
    ST_CENTROID(zcta.geom) AS centroid
FROM census.zcta
INNER JOIN census.ameren_zcta ON CAST(ameren_zcta.zcta AS BIGINT) = CAST(zcta.geoid AS BIGINT)
 INNER JOIN (
    SELECT ST_Transform(ST_Union(geom),3435) as geom FROM census.service_boundary_test
  ) as boundary ON ST_Intersects(zcta.geom, boundary.geom)
EOM


remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "zcta" "4" "13" "$census_zcta_query"
add_layer "centroid" "1" "13" "$zcta_centroid_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Ameren Illinois Community Partner Data Portal Census Tiles" \
    "Census data for the Ameren Illinois Community Partner Data Portal" \
    "4" "13"


