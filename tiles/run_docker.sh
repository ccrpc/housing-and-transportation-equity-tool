#!/bin/bash

docker build -t lui.tiles:export .
docker run -v $PWD/output:/data --env-file .env --name export_tiles lui.tiles:export
docker container rm export_tiles
docker image rm lui.tiles:export