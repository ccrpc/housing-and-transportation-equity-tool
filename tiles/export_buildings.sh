#!/bin/sh

GPKG=/data/buildings.gpkg
MBTILES=/data/buildings.mbtiles

# Include helpers
source /tiles/tile_helpers.sh

read -r -d '' building_query <<EOM
SELECT geom,building_id,image_url,lbcs_structure::TEXT,parcel_pin::TEXT,stories,street_name::TEXT,units,front_setback,left_setback,rear_setback,right_setback,date::TEXT,last_edited,external_comment::TEXT
    FROM land_use.building
    WHERE study_area = 'true'
EOM

remove_file "$GPKG"
remove_file "$MBTILES"
add_layer "building" "12" "22" "$building_query"

# Convert GeoPackage to MBTiles
export_tiles \
    "Land Use Inventory Buildings" \
    "Buildings from the Land Use Inventory Project" \
    "12" "22"
