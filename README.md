# Socio-economic Impact Tool

### Description

(also wiki link?)

### Wireframe

The wireframe for the project is available here: https://app.diagrams.net/#G1uL8taHmMBVP4BQMWwgjvv8e_hqPfRhQX#%7B%22pageId%22%3A%22DxCF8aZOM_kGBYS2OKD-%22%7D

Note that you will need to be logged into the Data and Technology Google Account to view it.

#TODO: Make the wireframe more available, mayb export it as an image and store it in the repository.

## How to convert geopackage to mbtiles

In the folder where you have the geopackage file, run `docker run --rm -it -v $PWD:/app ghcr.io/osgeo/gdal:alpine-normal-latest sh`

And inside of that container do `cd app` and then run the command:

```
ogr2ogr -f MBTILES "socio-economic-impact-tool.mbtiles" "Socio-Economic Impact Tool.gpkg" \
    -progress \
    -dsco NAME="Socio-economic Impact Tool Layers" \
    -dsco DESCRIPTION="Socio-economic Impact Tool Layers" \
    -dsco MINZOOM="0" \
    -dsco MAXZOOM="14" \
    -dsco CONF="{\"building\":{\"target_name\":\"socio_economic_impact_tool_buildings\",\"minzoom\":0,\"maxzoom\":14}}"
```

If docker is not installed / enabled one can also do it with podman: `podman run --rm -it -v \$PWD:/app:Z ghcr.io/osgeo/gdal:alpine-normal-latest sh`

## Updating the Database with the Python script

In order to run ht_data_update.py you will need to make sure you have all the prequisites install in your python environment.

`pip install requests pandas geopandas python-dotenv sqlalchemy psycopg2-binary`

Next copy .env.example and rename it to .env, the required information can be found in the wiki and password database.

Data is being pulled from the US Census ACS5. The ACS5 has data split between tables and geometries.
The three possible tables are data profile, detailed table, and subject table. Each has a unique Census API link.
The current geometries being pulled are census tracts and block groups. Each has a unique TIGER API link.

The script pulls all requested variables and then uploads it to the database, replacing the current one. When adding variables do not delete the existing ones.
The list of variables and their labels should be maintained at L:/CUUATS/Housing and Transportation Affordability and Accessibility Index/Data/Equity Tool Data.
Follow the naming convention laid out in the script's comments when adding new variables, they should be the variables codes listed separated only by columns.
They should have corresponding labels in the M column of the aforementioned spreadsheet. The labels cannot exceed 63 characters in length and must all be unique.
The variables cannot be in groups larger than 50. Once the cap of 50 has been reached create a new grouping while following the same naming convention. When there
is an error in the script, it is most likely the variable grouping causing issues.

After these are set, run the script and the everything should be updated in the database.
