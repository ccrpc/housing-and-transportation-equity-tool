
export const config = {
  namespace: 'socio-economic-impact-tool',
  outputTargets: [
    {
      type: 'www',
      baseUrl: '/socio-economic-impact-tool/',
      serviceWorker: null
    }
  ],
  plugins: [],
  globalScript: 'src/global/app.ts',
  globalStyle: 'src/global/app.css'
};