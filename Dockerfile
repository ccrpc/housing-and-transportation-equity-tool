FROM node:lts-alpine

WORKDIR /app

RUN corepack enable

COPY package.json .
COPY yarn.lock .
COPY .yarnrc.yml .

RUN yarn install --immutable

COPY . .

CMD ["yarn", "run", "start"]